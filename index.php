<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $sheep = new Animal("Shaun");
    echo "Name  = " . $sheep->name . "<br>" ;
    echo "Leg  = " . $sheep->leg . "<br>" ;
    echo "Cold Blooded  = " . $sheep->cold_blooded . "<br><br>"  ; 

    $frog = new Frog("Buduk");
    echo "Name  = " . $frog->name . "<br>" ;
    echo "Leg  = " . $frog->leg . "<br>" ;
    echo "Cold Blooded  = " . $frog->cold_blooded . "<br>" ;
    echo "Jump = ";
    echo $frog->Jump() . "<br><br>"; 

    $sungokong = new Ape("Kera sakti");
    echo "Name  = " . $sungokong->name . "<br>" ;
    echo "Leg  = " . $sungokong->leg . "<br>" ;
    echo "Cold Blooded  = " . $sungokong->cold_blooded . "<br>" ;
    echo "Jump = ";
    echo $sungokong->yell() . "<br>"; 







?>